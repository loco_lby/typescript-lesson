"use strict";
const oldVw = {
    name: 'safari',
    year: new Date(),
    broken: false,
    summary() {
        return this.name;
    },
};
const printVehicle = (vehicle) => {
    console.log(vehicle.name);
    console.log(vehicle.summary());
    //if no return, void
};
printVehicle(oldVw);
