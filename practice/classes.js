"use strict";
class Car {
    //   color: string;
    //   constructor(color: string) {
    //     this.color = color;
    //   }
    constructor(color) {
        this.color = color;
    }
    honk() {
        console.log('honk honk');
    }
}
const vehicle1 = new Car('orange');
// vehicle1.honk();
console.log(vehicle1.color);
class Vw extends Car {
    //overwrite method
    drive() {
        console.log('vroom');
    }
    startDriving() {
        this.drive();
        this.honk();
    }
    constructor(wheels, color) {
        super(color);
    }
}
const vehicle = new Vw(4, 'orange');
vehicle.startDriving();
