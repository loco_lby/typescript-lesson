const carMakers: string[] = [];

const dates = [new Date(), new Date()];

const carsByMake: string[][] = [];
const carsByMakeInference = [['corolla']['f150']['e series']];

// help with inference when extraxting  values

const car = carMakers[0];

// prevent incompatible values

// carMakers.push(100)

// help with maps

carMakers.map((car: string) => {
  return car.toLowerCase;
});

// flexible types

const importantDatesInference = [new Date(), '10-10-2001'];
const importantDates: (string | Date)[] = [];
// importantDates.push(100)
