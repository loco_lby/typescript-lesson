"use strict";
const add = (a, b) => {
    return a + b;
};
function divide(a, b) {
    return a / b;
}
const subtract = function (a, b) {
    return a - b;
};
const logger = (message) => {
    console.log(message);
    //if no return, void
};
const forecast = {
    date: new Date(),
    weather: ['sunny', 'rainy'],
};
const logWeather = (forecast) => {
    console.log(forecast.weather);
};
const logWeather2 = ({ date, weather, }) => {
    console.log(weather);
};
logWeather2(forecast);
