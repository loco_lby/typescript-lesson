"use strict";
const profile = {
    name: 'john',
    age: 20,
    coords: {
        lat: 20,
        lng: 30,
    },
    setAge(age) {
        this.age = age;
    },
};
const { age } = profile;
// error is ok
const { coords: { lat, lng }, } = profile;
