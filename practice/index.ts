import axios from 'axios';

const url = 'https://jsonplaceholder.typicode.com/todos/1';

interface Todo {
  id: number;
  title: string;
  completed: boolean;
}

axios.get(url).then((response) => {
  const todo = response.data as Todo;

  const id = todo.id;
  const title = todo.title;
  const completed = todo.completed;

  logTodo(id, title, completed);
});

const logTodo = (id: number, title: string, completed: boolean) => {
  console.log(`${id}, ${title}, ${completed}`);
};

let apples: number = 5;

let colors: string[] = ['string1', 'string2'];

colors = ['black', 'blue'];

console.log(colors);

//classes

class Car {}

let car: Car = new Car();

// object literal

let points: { x: number; y: number } = {
  x: 10,
  y: 100,
};

const logNumber: (i: number) => void = (i: number) => {
  console.log(i);
};

//any

const json = '{"x": 10, "y": 20}';
const coords: { x: number; y: number } = JSON.parse(json);

console.log(coords);

// reinitialize

let words = ['red', 'green', 'blue'];
let foundWord: boolean;

for (let index = 0; index < words.length; index++) {
  if (words[index] === 'green') {
    foundWord = true;
    console.log(foundWord);
  }
}

// false inferrence

let numbers = [-10, -5, 0, 4, 6, 72];
let positiveNumber: boolean | number = false;

for (let index = 0; index < numbers.length; index++) {
  if (numbers[index] > 0) {
    positiveNumber = numbers[index];
    console.log(positiveNumber);
  }
}
