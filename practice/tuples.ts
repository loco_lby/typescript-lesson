// object

const soda = {
  color: 'brown',
  carbonated: true,
  sigar: 40,
};

let cola: [string, boolean, number] = ['brown', true, 40];
// cola[0] = 40

//or Type alias

type Drink = [string, boolean, number];

let pepsi: Drink = ['brown', true, 40];
let sprite: Drink = ['clear', true, 60];
