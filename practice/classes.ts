class Car {
  //   color: string;
  //   constructor(color: string) {
  //     this.color = color;
  //   }
  constructor(public color: string) {}

  protected honk(): void {
    console.log('honk honk');
  }
}

const vehicle1 = new Car('orange');
// vehicle1.honk();
console.log(vehicle1.color);

class Vw extends Car {
  //overwrite method
  private drive(): void {
    console.log('vroom');
  }

  startDriving(): void {
    this.drive();
    this.honk();
  }

  constructor(wheels: number, color: string) {
    super(color);
  }
}

const vehicle = new Vw(4, 'orange');

vehicle.startDriving();
