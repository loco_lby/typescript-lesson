interface Vehicle {
  name: string;
  year: Date;
  broken: boolean;
  summary(): string;
}

const oldVw = {
  name: 'safari',
  year: new Date(),
  broken: false,
  summary(): string {
    return this.name;
  },
};

const printVehicle = (vehicle: Vehicle): void => {
  console.log(vehicle.name);
  console.log(vehicle.summary());
  //if no return, void
};

printVehicle(oldVw);
