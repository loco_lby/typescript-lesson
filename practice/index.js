"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const url = 'https://jsonplaceholder.typicode.com/todos/1';
axios_1.default.get(url).then((response) => {
    const todo = response.data;
    const id = todo.id;
    const title = todo.title;
    const completed = todo.completed;
    logTodo(id, title, completed);
});
const logTodo = (id, title, completed) => {
    console.log(`${id}, ${title}, ${completed}`);
};
let apples = 5;
let colors = ['string1', 'string2'];
colors = ['black', 'blue'];
console.log(colors);
//classes
class Car {
}
let car = new Car();
// object literal
let points = {
    x: 10,
    y: 100,
};
const logNumber = (i) => {
    console.log(i);
};
//any
const json = '{"x": 10, "y": 20}';
const coords = JSON.parse(json);
console.log(coords);
// reinitialize
let words = ['red', 'green', 'blue'];
let foundWord;
for (let index = 0; index < words.length; index++) {
    if (words[index] === 'green') {
        foundWord = true;
        console.log(foundWord);
    }
}
// false inferrence
let numbers = [-10, -5, 0, 4, 6, 72];
let positiveNumber = false;
for (let index = 0; index < numbers.length; index++) {
    if (numbers[index] > 0) {
        positiveNumber = numbers[index];
        console.log(positiveNumber);
    }
}
