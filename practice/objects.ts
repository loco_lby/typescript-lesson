const profile = {
  name: 'john',
  age: 20,
  coords: {
    lat: 20,
    lng: 30,
  },
  setAge(age: number): void {
    this.age = age;
  },
};

const { age }: { age: number } = profile;
// error is ok
const {
  coords: { lat, lng },
}: { coords: { lat: number; lng: number } } = profile;
