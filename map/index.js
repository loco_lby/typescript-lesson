"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Map_1 = require("./Map");
const User_1 = require("./User");
const Company_1 = require("./Company");
/// <reference types="@types/google.maps" />
const user = new User_1.User();
const company = new Company_1.Company();
const map = new Map_1.Map('map');
map.addMarker(user);
map.addMarker(company);
