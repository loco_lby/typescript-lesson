import { Map } from './Map';
import { User } from './User';
import { Company } from './Company';

/// <reference types="@types/google.maps" />

const user = new User();
const company = new Company();

const map = new Map('map');

map.addMarker(user);
map.addMarker(company);
