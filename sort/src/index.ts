import { NumbersCollection } from './NumbersCollection';
import { CharactersCollection } from './CharactersCollection';
import { LinkedList } from './LinkedList';

const numbers = new NumbersCollection([10, -3, 5, 0]);
const characters = new CharactersCollection('XyrghjEfhj');
const links = new LinkedList();
links.add(500);
links.add(-10);
links.add(-30);
links.add(43);

numbers.sort();
characters.sort();
links.sort();

console.log(numbers.data);
console.log(characters.data);
links.print();
